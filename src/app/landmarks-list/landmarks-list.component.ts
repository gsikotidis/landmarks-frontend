import { Component, OnInit } from '@angular/core';
import * as Parse from 'parse';

@Component({
  selector: 'app-landmarks-list',
  templateUrl: './landmarks-list.component.html',
  styleUrls: ['./landmarks-list.component.scss']
})
export class LandmarksListComponent implements OnInit {

  constructor() { }

  public landMarks = [];
  public sortedLands = [];
  public currentUsername: any;
  public admin: any;

  ngOnInit() {

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
    const query = new Parse.Query(DubaiLandmarks);
    // tslint:disable-next-line: only-arrow-functions
    query.find().then((data: any) => {
      // tslint:disable-next-line: prefer-for-of
      for (let _i = 0; _i < data.length; _i++) {
        this.landMarks.push(data[_i].attributes);
      }
      this.landMarks.sort((a, b) => {
        return a.order - b.order;
      });

    });

    try {
      const currentUser = Parse.User.current();
      this.currentUsername = currentUser.getUsername();
      if (this.currentUsername === 'admin') {
        this.admin = true;
      }
    } catch (error) {
      console.log('No username in session');
    }

  }

}
