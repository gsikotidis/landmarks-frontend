import { Component, OnInit } from '@angular/core';
import * as Parse from 'parse';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-landmark',
  templateUrl: './edit-landmark.component.html',
  styleUrls: ['./edit-landmark.component.scss']
})
export class EditLandmarkComponent implements OnInit {

  public landMarks = [];
  editForm: FormGroup;
  public currentUsername: any;
  public admin: any;
  public loading;
  selectedFile: File;

  // tslint:disable-next-line: variable-name
  constructor(private _activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.loading = false;
    this.editForm = this.formBuilder.group({
      short_info: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      url: ['', Validators.required]
    });

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
    const query = new Parse.Query(DubaiLandmarks);
    query.equalTo('order', +this._activatedRoute.snapshot.params.id);
    // tslint:disable-next-line: only-arrow-functions
    query.find().then((data: any) => {
      // tslint:disable-next-line: prefer-for-of
      for (let _i = 0; _i < data.length; _i++) {
        this.landMarks.push(data[_i].attributes);
      }
    });
    console.log(this.landMarks);

    try {
      const currentUser = Parse.User.current();
      this.currentUsername = currentUser.getUsername();
      if (this.currentUsername === 'admin') {
        this.admin = true;
      }
      console.log(this.currentUsername);
    } catch (error) {
      console.log('No username in session');
    }
    console.log(this.admin);

  }

  get f() { return this.editForm.controls; }

  async submitChanges() {

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
    const query = new Parse.Query(DubaiLandmarks);
    query.equalTo('order', +this._activatedRoute.snapshot.params.id);
    const results = await query.find();

    if (this.f.title.value) {
      results[0].set('title', this.f.title.value);
      results[0].save();
    }
    if (this.f.short_info.value) {
      results[0].set('short_info', this.f.short_info.value);
      results[0].save();
    }
    if (this.f.url.value) {
      results[0].set('url', this.f.url.value);
      results[0].save();
    }
    if (this.f.description.value) {
      results[0].set('description', this.f.description.value);
      results[0].save();
    }
    location.reload();
  }

  async onFileChanged(event, order) {

    this.loading = true;
    this.selectedFile = event.target.files[0];

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
    const query = new Parse.Query(DubaiLandmarks);
    query.equalTo('order', order);
    const results = await query.find();

    const parseFile = new Parse.File(this.selectedFile.name, this.selectedFile);
    results[0].set('photo', parseFile);
    results[0].save().then((data: any) => {
      this.http.get('http://localhost:1337/resize',
        {
          params: {
            searchKey: order
          }
        }).subscribe(() => {
        });
      this.loading = false;
      location.reload();
    }, (data: any) => {
      // The file either could not be read, or could not be saved to Parse.
    });

  }

}
