## landmarks-frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## How to run

1.  Go to [landmarks-backend](https://gitlab.com/gsikotidis/landmarks-backend) project and follow the instructions to start the backend server.
2.  In root dir run `npm i`.
3.  Run `ng serve`.  Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
