import { Component, OnInit, Input } from '@angular/core';
import * as Parse from 'parse';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-landmark',
  templateUrl: './landmark.component.html',
  styleUrls: ['./landmark.component.scss']
})
export class LandmarkComponent implements OnInit {

  public landMarks = [];
  public currentUsername: any;
  public admin: any;
  // tslint:disable-next-line: variable-name
  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
    const query = new Parse.Query(DubaiLandmarks);
    query.equalTo('order', +this._activatedRoute.snapshot.params.id);
    // tslint:disable-next-line: only-arrow-functions
    query.find().then((data: any) => {
      // tslint:disable-next-line: prefer-for-of
      for (let _i = 0; _i < data.length; _i++) {
        this.landMarks.push(data[_i].attributes);
      }
    });
    try {
      const currentUser = Parse.User.current();
      this.currentUsername = currentUser.getUsername();
      if (this.currentUsername === 'admin') {
        this.admin = true;
      }
    } catch (error) {
      console.log(error);
    }

  }

}
