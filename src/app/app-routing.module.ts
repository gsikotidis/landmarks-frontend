import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandmarksListComponent } from './landmarks-list/landmarks-list.component';
import { LandmarkComponent } from './landmarks-list/landmark/landmark.component';
import { EditLandmarkComponent } from './landmarks-list/edit-landmark/edit-landmark.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        pathMatch: 'full',
        component: LandmarksListComponent
      }
    ]
  },
  {
    path: 'list/:id',
    children: [
      {
        path: '',
        redirectTo: 'landmark',
        pathMatch: 'full',
      },
      {
        path: 'landmark',
        pathMatch: 'full',
        component: LandmarkComponent
      },
      {
        path: 'edit',
        pathMatch: 'full',
        component: EditLandmarkComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
