import { Component, OnInit } from '@angular/core';
import * as Parse from 'parse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public currentUsername: any;
  public admin: any;
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  async ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    try {
      const currentUser = Parse.User.current();
      this.currentUsername = currentUser.getUsername();
      if (this.currentUsername === 'admin') {
        this.admin = true;
      }
    } catch (error) {
      console.log(error);
    }
  }

  get f() { return this.loginForm.controls; }

  async validateLogin() {

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');

    try {
      await Parse.User.logIn(this.f.username.value, this.f.password.value);
      console.log('success');
    } catch (error) {
      alert('Error: ' + error.code + ' ' + error.message);
    }
    location.reload();
  }

  async validateLogout() {

    Parse.serverURL = 'http://localhost:1337/parse';
    Parse.initialize('myAppId');
    Parse.User.logOut();
    location.reload();
  }

}
